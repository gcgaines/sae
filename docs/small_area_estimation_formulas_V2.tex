%%%%%%%%  Document class  %%%%%%%%%%%%
\documentclass[10pt, letterpaper, fleqn]{article}
% fleqn is for left aligned equations

%%%%%%%%  Packages   %%%%%%%%%%%%%%%%
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{times}
\usepackage{array}
\usepackage{esint}
\usepackage{enumerate}
\usepackage{bm}
\usepackage{commath}
\usepackage{stackengine}


%%%%%%%%  Page Setup %%%%%%%%%%%%%%%%%%
\usepackage[margin= 1.5cm]{geometry}

%%%%%%%% Document %%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{center}
\textbf{\LARGE Small Area Estimation} \\[18pt]
\end{center}

\noindent
\textbf{\Large Formulas used in project for Graduate Applied Stat Seminar, Fall 2018}\\[4pt]

\noindent
This document tracks the forms of the estimators I've used throughout my class project for STAT 640: Graduate Seminar in Probability and Statistics. I used data from a stem-mapped forest stand in New Jersey for this project. The objective is to evaluate the relative performance of a range of estimators of Domain total tree volume.\\[1pt]

\noindent
\textbf{\large Some Notation}\\[4pt]

\noindent
$D_i$ = Domain $i$; a subset of the population \\[1pt]

\noindent
$n$ = total sample size \\[1pt]

\noindent
$n_i$ = number of sample elements in $D_i$\\[1pt]

\noindent
$N$ = total number of elements in the population\\[1pt]

\noindent
$N_i$ = total number of population elements in $D_i$\\[1pt]

\noindent
$\pi_k$ = element inclusion probability; the probability, or relative
frequency with which, population element $U_k$ is included in a sample.\\[1pt]

\noindent
$w_k$ = notation used in [2] to denote the inverse of the inclusion probability, i.e. $\frac{1}{\pi_k}$.\\[1pt]

\noindent
$p(s_k)$ = sample selection probability. The probability, or relative frequency with which, sample $k$ is selected. In SRS, $\pi_k$ are equal and $p(s_k)$ are equal for all possible samples of a fixed size $n$.\\[1pt]

\noindent
$f = \frac{n}{N}$ = the sampling fraction. \\[1pt]

\noindent
\textbf{\emph{Throughout the remainder of this document,}} subscripts on the estimated total symbol $\hat\tau$ will categorize estimators as follows (except when reproducing equations from references):\\[1pt]

\noindent
$d \rightarrow$ direct domain estimation \hspace{1cm} $s \rightarrow$ synthetic (i.e. indirect) domain estimation \hspace{1cm} $c \rightarrow$ composite domain estimation\\[1pt]

\noindent
$\pi \rightarrow$ simple expansion estimator \hspace{1cm} \emph{RAT} $\rightarrow$ ratio estimator \hspace{1cm} \emph{REG} $\rightarrow$ regression estimator \\[1pt]

\noindent
\textbf{\large{Expansion estimator for a population total}}\\[1pt]

\noindent
Recall from [1] an unbiased expansion estimator for a population total under SRSwoR:

\begin{align}
\hat\tau_{y \pi} = \sum\limits_{U_k \in s} \frac{y_k}{\pi_k} = \frac{N}{n} \sum\limits_{U_k \in s} y_k = N\bar{y}.\label{eq1}
\end{align}

\noindent
To demonstrate the unbiasedness of \ref{eq1} as an estimator of $\tau_y$, i.e., that $E[\hat\theta] = \theta \rightarrow E[\hat\tau_{y \pi}] = \tau_y$, we must calculate $E[\hat\tau_{y \pi}]$ the expected value of $\hat\tau_{y \pi}$:
\begin{align}
E(\hat\tau_{y \pi}) = \sum\limits_{s \in \Omega} p(s) \hat\tau_{y \pi}(s) = \sum\limits_{s \in \Omega} \left[p(s) \sum\limits_{U_k \in s} \frac{y_k}{\pi_k}\right] = \sum\limits_{s \in \Omega} \left[\sum\limits_{s \ni U_k} p(s) \frac{y_k}{\pi_k}\right] & = \sum\limits_{k = 1}^N \frac{y_k}{\pi_k} \left[\sum\limits_{s \ni U_k} p(s)\right] \\ \nonumber
& = \sum\limits_{k = 1}^N \frac{y_k}{\pi_k} \pi_k \\ \nonumber
& = \sum\limits_{k = 1}^N y_k = \tau_y. \nonumber
\end{align}

\noindent
Because $E[\hat\tau_{y \pi}] = \tau_y$, the bias of $\hat\tau_{y \pi}$ as an estimator of $\tau_y$ is $B[\hat\tau_{y \pi} : \tau_y] = E[\hat\tau_{y \pi}] - \tau_y = 0$, and thus $\hat\tau_{y \pi}$ is an unbiased estimator for $\tau_y$.\\[1pt]

\noindent
\textbf{An alternative proof using a useful technique:}\\[1pt]

\noindent 
Define the random variable $I_k$ as a binary indicator variable that indicates whether population unit $U_k$ was included in a sample:
\begin{align}
I_k = \left\{\begin{array}{cl}
        1 & \ \text{if} \ U_k \ \in s \\
        0 & \ \text{otherwise} \end{array} \right.
\end{align}

\noindent
Then \eqref{eq1} can be written as \\[1pt]
\begin{align}
\hat\tau_{y \pi} = \sum\limits_{k = 1}^N \frac{y_k I_k}{\pi_k}
\end{align}

\noindent
In this expression $I_k$ is the only random term, as its value indicates whether $U_k$ was drawn into a random sample. The $y_k$ value associated with $U_k$ is regarded as a fixed value, irrespective of whether $U_k$ was selected into a particular sample. Likewise, $\pi_k$ is design-based, and is also independent of the sample chosen. \\[1pt]

\noindent
The expected value of $I_k$ is
\begin{align}
E[I_k] = 0 \times \text{Prob}[I_k = 0] + 1 \times \text{Prob}[I_k = 1] = 0 + \pi_k = \pi_k.
\end{align}

\noindent
Thus, the expected value of $\hat\tau_{y \pi}$ is 
\begin{align}
E[\hat\tau_{y \pi}] = E\left[\sum\limits_{k = 1}^N \frac{y_k I_k}{\pi_k}\right] = \sum\limits_{k = 1}^N \frac{y_k E[I_k]}{\pi_k} = \sum\limits_{k = 1}^N \frac{y_k \pi_k}{\pi_k} = \sum\limits_{k = 1}^N y_k = \tau_y.
\end{align}

\noindent
Pretttty cool!\\[1pt]

\noindent
Additionally, the variance of $\hat\tau_{y \pi}$ is obtained from the definition of the variance $V[\hat\tau_{y \pi}] = E[\hat\tau^2_{y \pi}] - E[\hat\tau_{y \pi}]^2$ as
\begin{align}
V[\hat\tau_{y \pi}] = V[N\bar{y}] = N^2V[\bar{y}] = N^2 \sum\limits_{s \in \Omega} \frac{1}{\Omega}(\bar{y}(s) - \mu(y))^2 = N^2(\frac{1}{n} - \frac{1}{N})\sigma^2_y.
\end{align}

\noindent
When the population is large, $\frac{1}{N} \approx 0$ and thus $V[\hat\tau_{y \pi}] \approx \frac{N^2 \sigma^2_y}{n}$.\\[1pt]

\noindent
\textbf{\large{Domain estimation: expansion estimators under SRSwoR}}\\[1pt]

\noindent
Expansion estimators can be used in the absence of full-coverage auxiliary information.\\[1pt]

\noindent
\textbf{Direct Expansion Estimator:}\\[1pt]

\noindent
Equation \ref{eq1} can be adapted to estimate domain totals, as in \texttt{direct.expansion} in R script \texttt{3.1 \textunderscore sample.gg.R}, by reformulating as
\begin{align}
\hat\tau_{\pi d_i} = \frac{N_i}{n_i} \sum\limits_{U_k \in s_i} y_k = N_i\bar{y_i}
\end{align}

\noindent
where $\bar{y_i}$ is the mean volume of sample trees in $D_i$. This was adapted to a domain context from equation 3.4 in [1].\\[1pt]
 
\noindent
Expected values and variances can be obtained via \\[1pt]
 
\noindent
$E[\hat\tau_{\pi d_i}] = \tau_{y_i} ?$\\[1pt]

\noindent
$V[\hat\tau_{\pi d_i}] = \frac{N^2_i \sigma^2_{y_i}}{n_i} ?$\\[1pt]

\noindent
\textbf{Synthetic Expansion Estimator:}
\begin{align}
\hat\tau_{\pi s_i} = \frac{N_i}{n} \sum\limits_{U_k \in s} y_k = N_i\bar{y}
\end{align}

\noindent
where $\bar{y}$ is the mean volume of all sample trees in \emph{s}.\\[1pt]

\noindent
Expected values and variances can be obtained via \\[1pt]

\noindent
$E[\hat\tau_{\pi s_i}] = E[\frac{N_i}{N} N\bar{y}] = \frac{N_i}{N} E[N\bar{y}] = \frac{N_i}{N} \tau_y ?$\\[1pt]

\noindent 
Note that $E[\hat\tau_{\pi s_i}] = \frac{N_i}{N} \tau_y \neq \tau_{y_i}$, showing the biasedness of $\hat\tau_{\pi s_i}$ as an estimator of $\tau_{y_i}$. \\[1pt]

\noindent
$V[\hat\tau_{\pi s_i}] = \frac{N^2_i \sigma^2_{y}}{n} ?$\\[1pt]

\noindent
\textbf{Sample-size Dependent (SSD) Weighted Composite Estimator of Direct and Synthetic Expansion Estimates:}\\[1pt]

\noindent
A general form for a composite estimator for the domain total in $D_i$ is
\begin{align}
\hat\tau_{c_i} = w_i \hat\tau_{1_i} + (1 - w_i) \hat\tau_{2_i} \label{comp1},
\end{align}

\noindent
or, using notation from equation 4.3.1 in [2],
\begin{align}
\hat{Y}_{ic} = \phi_i \hat{Y}_{i1} + (1 - \phi_i) \hat{Y}_{i2},
\end{align}

\noindent
where $\hat\tau_{1_i}$ may be some unbiased but highly variable and imprecise estimate of $\tau$, say $\hat\tau_{\pi d_i}$, and $\hat\tau_{2_i}$ is an alternative estimator, say $\hat\tau_{\pi s_i}$, which may be a biased but more stable estimator of $\tau$. \\[1pt]

\noindent
Many methods exist for the determination of the weights $w_i$ (or $\phi_i$), but the objective generally remains the same: to balance variance and bias by adaptively controlling the contributions of the component estimators to overall estimates of $\tau_i$.\\[1pt]

\noindent
The \texttt{ssd.expansion} estimator in \texttt{3.1\textunderscore sample.gg.R} employs weights $w_i$ ($\phi_i$) equivalent to 4.3.8 in [2]:
\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ \hat{N}_i / N_i \ \geq \delta \\
        \hat{N}_i / \delta N_i & \ \text{if} \ \hat{N}_i / N_i < \delta \end{array} \right.
\end{align}

\noindent
where $\hat{N}_i = \sum\limits_{s_i} \frac{1}{\pi_k} = \frac{N}{n} n_i$ is the direct expansion estimator of $N_i$ and $\delta$ is subjectively chosen to control the contribution of the synthetic estimator. In other words, if the ratio of estimated domain size (based on domain sample size $n_i$) to actual domain size is larger than some threshold $\delta$, we assign a full weight of 1 to the direct estimator. This happens when $n_i$ is sufficiently large relative to $E[n_i]$. If, however, the ratio of estimated domain size to actual domain size is smaller than $\delta$ (i.e., $n_i$ is not sufficiently large), then we lend weight of $\hat{N}_i / \delta N$ to the direct expansion estimator and of $1 - (\hat{N}_i / \delta N)$ to the synthetic estimator.\\[1pt]

\noindent
Common choices of $\delta$ are 1 and 2/3.\\[1pt]

\noindent 
Our SSD composite expansion estimator \texttt{ssd.expansion} in \texttt{3.1\textunderscore sample.gg.R} takes on the form 
\begin{align}
\hat\tau_{\pi c_i} = w_i \hat\tau_{\pi d_i} + (1 - w_i) \hat\tau_{\pi s_i}.
\end{align}

\noindent
Expected values and variances can be obtained via \\[1pt]

\noindent
$E[\hat\tau_{\pi c_i}] = ?$\\[1pt]

\noindent
$V[\hat\tau_{\pi c_i}] = ?$\\[1pt]

\noindent
\textbf{The optimal estimator: another approach to calculating $w_i \ (\phi_i)$:} \\[1pt]

\noindent
The bias of an estimator of a population parameter, or the difference between the expected value of the estimator from its target parameter, can be expressed as
\begin{align}
B[\hat\theta:\theta] = E[\hat\theta] - \theta.
\end{align}

\noindent
The variance of an estimator of a population parameter, or the average squared distance between individual estimates $\theta\hat(s)$ and their mean $E[\theta\hat]$ can be expressed as
\begin{align}
V[\hat\theta] = \sum\limits_{s \in \Omega} p(s) \left(\hat\theta(s) - E[\hat\theta] \right)^2.
\end{align}

\noindent
Using these definitions, the mean squared error (MSE) of an estimator can be calculated, according to Section 2.2.6 in [1], as
\begin{align}
MSE[\hat\theta:\theta] & = \sum\limits_{s \in \Omega} p(s) \left(\hat\theta(s) - \theta \right)^2 \nonumber \\
& = V[\hat\theta] + \left(B[\hat\theta:\theta]\right)^2.
\end{align}

\noindent
Thus, MSE is a measure both of how much $\hat\theta$ varies around its mean, and how distant it's mean is from the target parameter $\theta$. So, when $\hat\theta$ is unbiased, i.e. $B[\hat\theta:\theta] = E[\hat\theta] - \theta = 0$, then $MSE[\hat\theta:\theta] = V[\hat\theta]$. It's utility lies in it's ability to evaluate both sources of statistical error, rather than focusing exclusively on one or the other.\\[1pt]

\noindent
As outlined above, a viable way to balance the instability of a direct estimator such as $\hat\tau_{\pi d_i}$ with the bias of a synthetic estimator such as $\hat\tau_{\pi s_i}$ is to take a weighted average of the two. Section 4.3.1 of [2] illuminates the use of MSE as a tool to construct what it terms ``optimal weights'' $\phi_i$, as shown below, for composite estimators with the form of  \eqref{comp1}.\\[1pt]

\noindent
The \emph{design} (indicated by the subscript $p$) MSE of the composite estimator $\hat\tau_{c_i}$ constructed from componenet estimators $\hat\tau_{1_i}$ and $\hat\tau_{2_i}$ is given by
\begin{align}
MSE_p[\hat\tau_{c_i}] = \phi^2_i MSE_p[\hat\tau_{1_i}] + (1 - \phi_i)^2 MSE_p[\hat\tau_{2_i}] + 2 \ \phi_i (1 - \phi_i) \ E_p[(\hat\tau_{1_i} - \tau_i)(\hat\tau_{2_i} - \tau_i)] \label{mse.comp}
\end{align}

\noindent
By minimzing \eqref{mse.comp} with respect to $\phi_i$ (i.e., taking the derivative with respect to $\phi_i$, setting it equal to 0, and soliving for $\phi_i$), we get the ``optimal'' weight $\phi_i$ as
\begin{align}
\phi^*_i = \frac{MSE_p[\hat\tau_{2_i}] - E_p[(\hat\tau_{1_i} - \tau_i)(\hat\tau_{2_i} - \tau_i)]}{MSE_p[\hat\tau_{1_i}] + MSE_p[\hat\tau_{2_i}] - 2E_p[(\hat\tau_{1_i} - \tau_i)(\hat\tau_{2_i} - \tau_i)]}
\end{align}

\noindent
which, assuming that the covariance term $E_p[(\hat\tau_{1_i} - \tau_i)(\hat\tau_{2_i} - \tau_i)]$ is small relative $MSE_p[\hat\tau_{2_i}]$, is
\begin{align}
\approx \frac{MSE_p[\hat\tau_{2_i}]}{MSE_p[\hat\tau_{1_i}] + MSE_p[\hat\tau_{2_i}]} \label{approx.opt.weights}
\end{align}

\noindent
This shows that the ``approximate optimal weights'' $\phi^*_i$ given by \eqref{approx.opt.weights} fall on the interval [0,1], and depend only the ratio of the MSEs of the component estimators:
\begin{align}
\phi^*_i = 1/1+F_i,
\end{align}

\noindent
Where $F_i = MSE_p[\hat\tau_{1_i}] / MSE_p[\hat\tau_{2_i}]$. Moreover, $MSE_p[\hat\tau_{c_i}]$ with optimal weights $\phi^*_i$ further reduces to 
\begin{align}
MSE_p[\hat\tau_{c_i}] = \phi^*_i MSE_p[\hat\tau_{1_i}] = (1 - \phi^*_i) MSE_p[\hat\tau_{2_i}].
\end{align}

\noindent
It follows that the reduction in MSE by the optimal estimator relative to the smaller of the MSEs of the component estimators is given by $\phi^*_i$ if $0 \leq \phi^*_i \leq 1/2$, and it equals $1 - \phi^*_i$ if $1/2 \leq \phi^*_i \leq 1$. Thus the maximum reduction of 50\% is acheived when $\phi^*_i = 1/2 (F_i = 1)$.\\[1pt]

\noindent
It can be shown that $\hat\tau_{c_i}$ is better than either component estimator in terms of MSE when $max(0,2 \phi^*_i - 1) \leq \phi_i \leq min(2 \phi^*_i, 1)$. The optimal weight $\phi^*_i$ will be close to zero or one when one of the component estimators has a much larger MSE than the other, i.e. when $F_i$ is either small or large. In this case, the estimator with larger MSE adds little information and it is better to use the esimator with smaller MSE rather than the component estimator. \\[1pt]

\noindent 
In practice (because we usually can't calculate the variance or bias of an estimator when we don't know $N$), we use a prior guess of the optimal value $\phi^*_i$, or we estimate it from the sample data.  Assuming that the direct estimator $\hat\tau_{\pi d_i}$ is either p-unbiased or approximately p-unbiased as the overall sample size increases, we can estiamte the optimal weight 
\begin{align}
\frac{MSE_p[\hat\tau_{2_i}]}{MSE_p[\hat\tau_{1_i}] + MSE_p[\hat\tau_{2_i}]}
\end{align}

\noindent
using
\begin{align}
\text{mse}(\hat\tau_{\pi s_i}) \approx (\hat\tau_{\pi s_i} - \hat\tau_{\pi d_i})^2 - v(\hat\tau_{\pi d_i})
\end{align}

\noindent
where mse denotes a p-unbiased estimator of $MSE_p$ (see section 4.2.4 of [2] for more information). We substitute the estimator mse$(\hat\tau_{2_i})$ for the numerator $MSE_p[\hat\tau_{2_i}]$ and $(\hat\tau_{\pi s_i} - \hat\tau_{\pi d_i})^2$ for the denominator $MSE_p[\hat\tau_{1_i}] + MSE_p[\hat\tau_{2_i}]$:
\begin{align}
\hat\phi^*_i = \frac{\text{mse}(\hat\tau_{2_i})}{(\hat\tau_{2_i} - \hat\tau_{1_i})^2}
\end{align}

\noindent
This estimator of $\phi^*_i$ can be highly unstable. Averaging estimated weights $\hat\phi^*_i$ over several variables or "similar" areas (?) can improve it to some extent.

\noindent
\textbf{\large Generalized ratio estimator of the total:}\\[1pt]

\noindent
Ratio estimators are a class of estimators that may increase the precision of our estimates of $\tau_i$ given the availability of full-coverage auxiliary information, $x$, which exhibits a strong, positive, linear relationship with $y$ which passes through the origin.\\[1pt]

\noindent 
The generalized ratio estimator of a population total $y$, as expressed in equation 6.1 in [1], takes on the form
\begin{align}
\tau_y = R_{y|x} \tau_x,
\end{align}
\noindent
where the population parameter $R_{y|x} = \tau_y/\tau_x$. This suggests, given that we know the true population total $\tau_x$ (and corresponding domain totals $\tau_{x_i}$) of some auxiliary variable $X$ with a strong, positive, linear relationship with $y$, that we can estimate $\tau_y$ as the product of some estimate of $R_{y|x}$ and $\tau_x$.\\[1pt]

\noindent
Under SRSwoR, one could estimate $R_{y|x}$ by the ratio of their Horvitz-Thompson estimators, as in equation 6.2 in [1] for a population total:
\begin{align}
\hat{R}_{y|x} = \frac{\tau_{y\pi}}{\tau_{x\pi}}.
\end{align}

\noindent
Or in equation 6.5 in [1] for a population mean:
\begin{align}
\hat{R}_{y|x} = \frac{\bar{y}}{\bar{x}}.
\end{align}

\noindent
\textbf{\large{Domain estimation: ratio estimators under SRSwoR}}\\[1pt]

\noindent
\textbf{Direct Ratio Estimator using basal area as the auxiliary variable \emph{x}:}\\[1pt] 

\noindent 
Our direct ratio estimator takes on the form of equation 6.3 in [1] adapted to the domain context by
\begin{align}
\hat\tau_{RATd_i} = \hat{R}_{y_i|x_i} \tau_{x_i} = \left(\frac{\hat{\tau}_{\pi d_i}(y)}{\hat\tau_{\pi d_i}(x)}\right) \tau_{x_i} = \frac{N_i\bar{y_i} \tau_{x_i}}{N_i\bar{x_i}}
\end{align}

\noindent
Expected values and variances can be obtained via \\[1pt]

\noindent
$E[\hat\tau_{RAT d_i}] = ?$\\[1pt]

\noindent
$V[\hat\tau_{RAT d_i}] = ?$\\[1pt]

\noindent
\textbf{Synthetic Ratio Estimator:}\\[1pt]

\noindent It follows that the indirect ratio estimator takes the form:

\begin{align}
\hat\tau_{RATs_i} = \hat{R}_{y_i|x_i} \tau_{x_i} = \left(\frac{\hat{\tau}_{\pi s_i}(y)}{\hat\tau_{\pi s_i}(x)}\right) \tau_{x_i} = \frac{N_i\bar{y} \tau_{x_i}}{N_i\bar{x}}
\end{align}

\noindent
Expected values and variances can be obtained via \\[1pt]

\noindent
$E[\hat\tau_{RAT s_i}] = ?$\\[1pt]

\noindent
$V[\hat\tau_{RAT s_i}] = ?$\\[1pt]


\noindent
\textbf{SSD Composite Ratio Estimator of Direct and Synthetic Ratio Estimates:}\\[1pt]

\noindent
And finally \texttt{ssd.ratio} in \texttt{3.1\textunderscore sample.gg.R} takes the form
\begin{align}
\hat\tau_{RAT c_i} = w_i \ \hat\tau_{RAT d_i} + (1 - w_i) \ \hat\tau_{RAT s_i}
\end{align}

\noindent
with weights 
\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ \hat{N}_i / N_i \ \geq \delta \\
        \hat{N}_i / \delta N_i & \ \text{if} \ \hat{N}_i / N_i < \delta \end{array} \right.
\end{align}

\noindent
and $\delta = 2/3$.\\[1pt]

\noindent
Expected values and variances can be obtained via \\[1pt]

\noindent
$E[\hat\tau_{RAT c_i}] = ?$\\[1pt]

\noindent
$V[\hat\tau_{RAT c_i}] = ?$\\[1pt]

\noindent
\textbf{\large Generalized Regression Estimator of the Total:}\\[1pt]

\noindent
When $y$ and $x$ are well-correlated but do not exhibit a linear relationship that can be characterized by a straight line passing through the origin, the regression estimator may be a viable alternative estimator of $\tau_y$. The data can be either positively or negatively correlated. Regression estimation then leverages the linear relationship described by the relationships
\begin{align}
y \approx \beta_0 + \beta_1 X_1 = \bm{Y} = \beta_0 + \bm{X \beta} \label{reg},
\end{align}

\noindent
where $B$ $(or \ \bm{\beta})$ is the slope of the line and $A$ $(or \ \beta_0)$ is the $y-$ intercept. Using simple linear least squares regression techniques as outlined in [3], we can estimate the coefficients using the following formulas
\begin{align}
& \hat\beta_1 = \frac{\sum\limits_{i = 1}^n (Y_i - \bar{y})(X_i - \bar{x})}{\sum\limits_{i = 1}^n (X_i - \bar{x})} \ \text{and} \nonumber \\ 
& \hat\beta_0 = \bar{Y} - \hat\beta_1 \bar{x} \hspace{.3cm} \text{or, in matrix notation,} \nonumber \\
& \hat\beta = (\bm{X}^T \bm{X})^{-1} \bm{X}^T \bm{Y}. \nonumber
\end{align}

\noindent 
where $\bm{X}$ is the $n \times p$ design matrix of auxiliary variables $X_1, X_2,...,X_p$, with a column of 1's in the first column in the event of an intercept other than 0, and $\bm{Y}$ is the $n \times 1$ column vector of observations of the target attribute.\\[1pt]

\noindent
Equation \eqref{reg} is a \emph{regression estimator} of $\tau_y$ if it is evaluated at $x = \tau_x$ with consistent estimates of $\beta_0$ and $\beta_1$.\\[1pt]

\noindent
\textbf{Regression estimation of the total under SRSwoR:}\\[1pt]

\noindent
Let
\begin{align}
s_{xy} = \frac{1}{n-1} \sum\limits_{U_k \in s} (y_k - \bar{y})(x_k - \bar{x}),  
\end{align}

\noindent
a design-unbiased estimator of the population covariance $\sigma_{xy}$, be the sample covariance between $x$ and $y$ and 
\begin{align}
s^2_x = \frac{1}{n-1} \sum\limits_{U_k \in s}(x_k - \bar{x})^2,
\end{align}

\noindent
a design-unbiased estimator of the population variance of $X$ $\sigma^2_x$, be the sample variance of $X$. The conventional estimator of of the slope of the line of interest can then be written as
\begin{align}
\hat\beta_1 = \frac{s_{xy}}{s^2_x}.
\end{align}

\noindent
Like $\hat{R}_{y | x}$, our estimator above for the population ratio $R_{y | x}$, $\hat\beta_1 = \frac{s_{xy}}{s^2_x}$ is a ratio of random variables and is therefore a biased estimator of the population parameter $\beta_1 = \frac{\sigma_{xy}}{\sigma^2_x}$.\\[1pt]

\noindent
Following SRSwoR, the estimator of the intecept is 
\begin{align}
\hat\beta_0 & = \frac{\hat\tau_y - \hat\beta_1 \hat\tau_x}{N} = \bar{y} - \hat\beta_1 \bar{x} \nonumber \\
& = \bar{y} - \hat\beta_1 \bar{x} 
\end{align}

\noindent
which can be thought of as an estimator of the population parameter $\beta_0 = \mu_y - \beta_1 \mu_x$. Both $\hat\beta_0$ and $\hat\beta_1$ are \emph{consistent} estimators of $\beta_0$ and $\beta_1$ respectively because, when $n = N$, $\hat\beta_0 = \beta_0$ and $\hat\beta_1 = \beta_1$.This leads to the conventional regression estimator of $\tau_y$  under SRSwoR\\[1pt]

\noindent
\textbf{Direct regression estimator using basal area as the auxiliary variable \emph{x}:}
\begin{align} 
\hat\tau_{REG d_i} & = N_i \hat\beta_{0_i} + \hat\beta_{1_i} \tau_{x_i} \nonumber \\
& = N_i [\bar{y}_i + \hat\beta_{1_i} (\mu_{x_i} - \bar{x}_i) \nonumber \\
& = \hat\tau_{\pi d_i}(y) + \hat\beta_{1_i} (\tau_{x_i} - \hat\tau_{\pi d_i}(x)).
\end{align}

\noindent
Expected values and variances can be obtained via \\[1pt]

\noindent
$E[\hat\tau_{REG d_i}] = ?$\\[1pt]

\noindent
$V[\hat\tau_{REG d_i}] = ?$\\[1pt]

\noindent
\textbf{\underline{References}}\\[1pt]

\noindent
[1] Gregoire, Timothy G., and Harry T. Valentine. \emph{Sampling strategies for natural resources and the environment.} Chapman and Hall/CRC, 2007.\\[1pt]

\noindent
[2] Rao, J. N. K. \emph{Small Area Estimation.} Hoboken: Wiley, 2003.\\[1pt]

\noindent
[3] DeGroot, Morris H., and Mark J. Schervish. \emph{Probability and statistics.} Pearson Education, 2012.\\[1pt]

\end{document}