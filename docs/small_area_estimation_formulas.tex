%%%%%%%%  Document class  %%%%%%%%%%%%
\documentclass[10pt, letterpaper, fleqn]{article}
% fleqn is for left aligned equations

%%%%%%%%  Packages   %%%%%%%%%%%%%%%%
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{times}
\usepackage{array}
\usepackage{esint}
\usepackage{enumerate}
\usepackage{bm}
\usepackage{commath}
\usepackage{stackengine}


%%%%%%%%  Page Setup %%%%%%%%%%%%%%%%%%
\usepackage[margin= 1.5cm]{geometry}

%%%%%%%% Document %%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{center}
\textbf{\LARGE Small Area Estimation} \\[18pt]
\end{center}

\noindent
\textbf{\Large Formulas used in project for Graduate Applied Stat Seminar, Fall 2018}\\[4pt]

\noindent
This document tracks the forms of the estimators I've used throughout my class project for STAT 640: Graduate Seminar in Probability and Statistics. I used data from a stem-mapped forest stand in New Jersey for this project. The objective is to evaluate the relative performance of a range of estimators of Domain total tree volume.\\[1pt]

\noindent
\textbf{\large Some Notation}\\[4pt]

\noindent
$D_i$ = Domain $i$; a subset of the population \\[1pt]

\noindent
$n$ = total sample size \\[1pt]

\noindent
$n_i$ = number of sample elements in $D_i$\\[1pt]

\noindent
$N$ = total number of elements in the population\\[1pt]

\noindent
$N_i$ = total number of population elements in $D_i$\\[1pt]

\noindent
$\pi_k$ = element inclusion probability; the probability, or relative
frequency with which, population element $U_k$ is included in a sample.\\[1pt]

\noindent
$w_k$ = notation used in [2] to denote the inverse of the inclusion probability, i.e. $\frac{1}{\pi_k}$.\\[1pt]

\noindent
$p(s_k)$ = sample selection probability. The probability, or relative frequency with which, sample $k$ is selected. In SRS, $\pi_k$ are equal and $p(s_k)$ are equal for all possible samples of a fixed size $n$.\\[1pt]

\noindent
$f = \frac{n}{N}$ = the sampling fraction. \\[1pt]

\noindent
\textbf{\large Direct Estimation of the Domain Total under SRSwoR}\\[2pt]

\noindent
\textbf{An unbiased expansion estimator for a population total under SRSwoR is} 

\begin{align}
\hat\tau_{y \pi} = \sum\limits_{U_k \in s} \frac{y_k}{\pi_k}.
\end{align}

\noindent
This can also be written as
\begin{align}
\hat\tau_{y \pi} = \frac{N}{n} \sum\limits_{U_k \in s} y_k = N\bar{y}.
\end{align}

\noindent
This can be adapted to estimate domain totals, as in \texttt{vol.est.1}, by reformulating as

\begin{align}
\hat\tau_{y \pi_i} = \frac{N_i}{n_i} \sum\limits_{U_k \in s_i} y_k = N_i\bar{y_i}.
\end{align}

\noindent
\textbf{\large Composite Estimation of the Domain Total under SRSwoR}\\[2pt]

\noindent
\textbf{A general form for a composite estimator for the total in $D_i$ is}
\begin{align}
\hat\tau_{y,comp_i} = w_i \hat\tau_{y \pi_i} + (1 - w_i) \hat\tau_{y \pi}
\end{align}

\noindent
The weights $w_i$ can be determined in many ways. In my first attempt at a direct composite estimator, the weights were comprised of a discrete binary indicator variable which assigned $i)$ the domain-specific expansion estimator to $D_i$ if contained more than 7 sample trees, or $ii)$ the mean all $n$ trees in the sample expanded by $N_i$ if $D_i$ contained 7 or fewer sample trees:

\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ 0 < n_i \leq 7 \\
        0 & \ \text{otherwise} \end{array} \right.
\end{align}

\noindent
This discrete discernment between $\hat\tau_{y \pi_i}$ and$\hat\tau_{y \pi}$ appeared to perform marginally better than using $\hat\tau_{y \pi_i}$ irrespective of sample size. Through visual assessment of simulation results, it seemed to do away with many of the exorbitantly high estimates that resulted from exclusive use of $\hat\tau_{y \pi_i}$. It seemed, however, that a smooth, continuous weight structure would perform better.\\[1pt]

\noindent
Thus we next considered weights of some form resembling
\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ n_i \ \text{``is real big''} \\
        \frac{n_i}{E(n_i)} & \ \text{if} \ n_i \ \text{is ``in between''} \\
        0 & \ \text{if} \ n_i = 0  \ \text{``or otherwise too small''} \end{array} \right.
\end{align}

\noindent
where $E(n_i) = \frac{N_i}{N} n$ under SRSwoR. For a first version of this, I considered

\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ n_i \ \leq 1.25 E(n_i) \\
        \frac{n_i}{E(n_i)} & \ \text{if} \ .75 E(n_i) \leq n_i < 1.25 E(n_i)\\
        0 & \ \text{if} \ n_i < .75 E(n_i) \end{array} \right.
\end{align}

but ended up first taking a crack at

\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ n_i \ \geq E(n_i)+2 s_{n_i} \\
        \frac{n_i}{E(n_i)} & \ \text{if} \ E(n_i) - 2 s_{n_i} \leq n_i < E(n_i) + 2 s_{n_i}\\
        0 & \ \text{if} \ n_i < E(n_i) - 2 s_{n_i} \end{array} \right.
\end{align}

where $s_{n_i}$ is the standard deviation of $n_i, \ \{i = 1,...,$ (total number of domains)\} from sample $s$?

None of this really made sense so I'm starting over here:\\[1pt]

\noindent
\textbf{Direct Expansion Estimator Under SRSwoR:}
\begin{align}
\hat\tau_{y \pi_i} = \frac{N_i}{n_i} \sum\limits_{U_k \in s_i} y_k = N_i\bar{y_i}
\end{align}

 where $\bar{y_i}$ is the mean volume of sample trees in $D_i$. This was adapted to a domain context from equation 3.4 in [1].\\[1pt]

\noindent
\textbf{Indirect Expansion Estimator:}
\begin{align}
\hat\tau_{y \pi_i, ind} = \frac{N_i}{n} \sum\limits_{U_k \in s} y_k = N_i\bar{y}
\end{align}

where $\bar{y}$ is the mean volume of all sample trees in \emph{s}.\\[1pt]

\noindent
\textbf{Sample-size Dependent (SSD) Weighted Composite Estimator of Direct and Indirect Expansion Estimates:}\\[1pt]

\noindent
A general form for a composite estimator for the total in $D_i$ is
\begin{align}
\hat\tau_{y,SSD_i} = w_i \hat\tau_{y \pi_i} + (1 - w_i) \hat\tau_{y \pi_i,ind},
\end{align}

\noindent
or, using notation from equation 4.3.1 in [2],
\begin{align}
\hat{Y}_{ic} = \phi_i \hat{Y}_{i1} + (1 - \phi_i) \hat{Y}_{i2},
\end{align}

\noindent
The \texttt{Sample-size dependent (SSD) weighted estimator of Direct \& Indirect expansion estimates} in \texttt{3.1 \textunderscore sample.gg.R} employs weights $w_i$ ($\phi_i$) equivalent to 4.3.8 in [2]:
\begin{align}
w_i = \left\{\begin{array}{cl}
        1 & \ \text{if} \ \hat{N}_i / N \ \geq \delta \\
        \hat{N}_i / \delta N & \ \text{if} \ \hat{N}_i / N < \delta \end{array} \right.
\end{align}

\noindent
where $\hat{N}_i = \sum\limits_{s_i} w_j = N \frac{n_i}{n}$ is the direct expansion estimator of $N_i$ and $\delta$ is subjectively chosen to control the contribution of the synthetic estimator. In other words, if the ratio of estimated domain size (based on domain sample size) to actual domain size is larger than $\delta$, we assign a full weight of 1 to the direct expansion estimator. This happens when $n_i$ is sufficiently large. If, however, the ratio of estimated domain size to actual domain size is smaller than $\delta$ (i.e., $n_i$ is not sufficiently large), then we lend weight of $\hat{N}_i / \delta N$ to the direct expansion estimator and of $1 - (\hat{N}_i / \delta N)$ to the indirect expansions estimator.\\[1pt]


\noindent
\textbf{\large Generalized Ratio Estimator of the Total:}\\[1pt]

\noindent 
\textbf{The generalized ratio estimator of a population total $y$,} as expressed in equation 6.1 in [1], takes on the form
\begin{align}
\tau_y = R_{y|x} \tau_x,
\end{align}
\noindent
where the population parameter $R_{y|x} = \tau_y/\tau_x$. This suggests, given that we know the true population total $\tau_x$ (and corresponding domain totals $\tau_{x_i}$) of some auxiliary variable $X$ with a strong, positive, linear relationship with $y$, that we can estimate $\tau_y$ as the product of some estimate of $R_{y|x}$ and $\tau_x$.\\[1pt]

\noindent
Under SRSwoR, one could estimate $R_{y|x}$ by the ratio of their Horvitz-Thompson estimators, as in equation 6.2 in [1] for a population total:
\begin{align}
\hat{R}_{y|x} = \frac{\tau_{y\pi}}{\tau_{x\pi}}.
\end{align}

\noindent
Or in equation 6.5 in [1] for a population mean:
\begin{align}
\hat{R}_{y|x} = \frac{\bar{y}}{\bar{x}}.
\end{align}

\noindent
\textbf{Direct Ratio Estimator Under SRSwoR, using basal area as the auxiliary variable \emph{n}:}\\[1pt] 

\noindent Thus, our direct ratio estimator takes on the form of equation 6.3 in [1] adapted to the domain context by
\begin{align}
\hat{\tau}_{y\pi,rat_i} = \hat{R}_{y_i|x_i} \tau_{x_i} = \hat{\tau}_{y\pi_i}\left(\frac{\tau_{x_i}}{\hat\tau_{x\pi_i}}\right) = \frac{\bar{y}_i}{\bar{x}_i} \tau_{x_i}
\end{align}

\noindent
\textbf{Indirect Ratio Estimator:}\\[1pt]

\noindent The indirect ratio estimator once again seeks to account for domains which may have insufficiently small samples by replacing $\bar{y}_i$ with $\bar{y}$ and $\bar{x}_i$ with $\bar{x}$:

\begin{align}
\hat{\tau}_{y\pi,rat_i,ind} = \hat{R}_{y|x} \tau_{x} = \hat{\tau}_{y\pi_i, ind}\left(\frac{\tau_{x_i}}{\hat\tau_{x\pi_i, ind}}\right) = \frac{\bar{y}}{\bar{x}} \tau_{x_i} = \frac{\bar{y}}{\bar{x}} N_i\mu_{x_i} = N_i \bar{y} \frac{\mu_{x_i}}{\bar{x}} = \hat{\tau}_{y\pi,ind} \frac{\mu{x_i}}{\bar{x}}.
\end{align}


\noindent
\textbf{SSD Weighted Ratio Estimator of Direct and Indirect Ratio Estimates:}\\[1pt]

\noindent
Thus, using the same weights $w_i$ as in $\hat\tau_{y,SSD_i}$, we can express our SSD ratio estimator as
\begin{align}
\hat\tau_{y SSD, rat_i} = w_i \hat\tau_{y \pi,rat_i} + (1 - w_i) \hat\tau_{y \pi,rat_i,ind}
\end{align}\\[1pt]

\noindent
\textbf{\large Generalized Regression Estimator of the Total:}\\[1pt]

\noindent
When $y$ and $x$ are well correlated but do not exhibit a linear relatinship that can be characterized by a straight line passing through the origin, the regression estimator may be a viable alternative estimator of $\tau_y$.

\noindent
\textbf{\underline{References}}\\[1pt]

\noindent
[1] Gregoire, Timothy G., and Harry T. Valentine. \emph{Sampling strategies for natural resources and the environment.} Chapman and Hall/CRC, 2007.\\[1pt]

\noindent
[2] Rao, J. N. K. \emph{Small Area Estimation.} Hoboken: Wiley, 2003.\\[1pt]

\end{document}