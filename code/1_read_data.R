code.dir<-"C:\\gaines\\projects\\sae\\code"
dat.dir<-"C:\\gaines\\projects\\sae\\data"

# New Jersey mapped stand data used in Gregoire and Scott boundary 
# overlap studies.
# dbh: 1/10's of inches
# ba: ft^2
# vol: ft^3

# Read in data
setwd(dat.dir)
stem<-read.table("NJ_stem_map.dat",sep="",skip=47,header=FALSE)
colnames(stem)<-c("block","line","spp","dbh","ba","volume","x","y")
stem$id<-1:nrow(stem)
stem<-stem[,c(9,1:8)]
spp.codes<-read.csv("FIATreeSpeciesCode.csv",header=TRUE)
spp.codes<-spp.codes[,1:2]
colnames(spp.codes)<-c("spp","common.name")
stem<-merge(stem,spp.codes,by="spp",all.x=FALSE)

# Convert dbh in 1/10 in to inches
stem$dbh<-stem$dbh/10

# Create FIA spp code factor variable to label barplot
stem$spp.fac<-as.factor(stem$spp)

# Remove 0-count species factor levels
stem$spp.fac<-droplevels(stem$spp.fac)
stem$common.name<-droplevels(stem$common.name)

# Shorten American hornbeam common name
levels(stem$common.name)[levels(stem$common.name)=="American hornbeam, musclewood"]<-"American hornbeam"

# Shorten other/unknown common name
levels(stem$common.name)[levels(stem$common.name)=="Other or unknown live tree"]<-"Other or unknown"

head(stem)

